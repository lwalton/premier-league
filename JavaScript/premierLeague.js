var fs = require("fs");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var data;
var actual = [];
var all = [];
var numGames;

getData();
dupe();
getScores();
printTable();

/**
 *  Gets and format data from files and online for league table
 */
function getData() {
    data = fs.readFileSync('prem.json');
    var jsonObject = JSON.parse(data);
    all = jsonObject.players;

    // console.log(all);
    var json_obj = JSON.parse(Get('https://www.footballwebpages.co.uk/league-table.json?comp=1'));
    numGames = json_obj.leagueTable.team[1].played;

    for (i1 = 0; i1 < 20; i1++) {
        actual[i1 + 1] = json_obj.leagueTable.team[i1].name;
        // console.log(i+1 + " " + actual[i+1]);
    }
}

/**
 * Pulls JSON data from yourURL
 * @param {*} yourUrl The URL of the JSON data
 */
function Get(yourUrl) {
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET", yourUrl, false);
    Httpreq.send(null);
    // console.log("RESPONSE TEXT: " + Httpreq.responseText);
    return Httpreq.responseText;
}

/**
 * Runs though all the users to check for duplicates
 */
function dupe() {
    for (i2 = 0; i2 < all.length; i2++) {
        checkForDuplicates(all[i2].table);
    }
}

/**
 * Checks if a given list has duplicates
 */
function checkForDuplicates(j) {
    for (i3 = 0; i3 < 19; i3++) {
        for (i4 = i3 + 1; i4 < 20; i4++) {
            if (j[i3] === j[i4]) {
                console.log("Duplicate found: " + j[i3] + " is the same as " + j[i4]);
                return;
            }
        }
    }
}

/**
 * Adds the score value to each json object
 */
function getScores() {
    for (i5 = 0; i5 < all.length; i5++) {
        all[i5].score = comp(all[i5].table, actual);
    }
}

/**
 * Calculates the score for the guess from the actual results.
 * @returns The score obtained for the user
 * @param {*} guess The guessed table
 * @param {*} actual The actual table
 */
function comp(guess, actual) {

    //COPIED FROM OLD CODE DIRECTLY
    var tempScore = 0;

    var errorBool = false;

    for (i = 1; i <= actual.length - 1; i++) {
        errorBool = false;
        for (j = 1; j <= guess.length - 1; j++) {
            if (actual[i] === guess[j]) {
                errorBool = true;
                if (i === j) {
                } else if (i > j) {
                    var diff = i - j;
                    tempScore += diff;
                } else if (j > i) {
                    var diff = j - i;
                    tempScore += diff;
                }
            }
        }
        if (errorBool === false) {
            //TODO This line doesnt work

            //TODO At least one table is missing some teams
            console.log("Team not found: " + actual[i].toString());
        }
    }
    return tempScore;
}

/**
 * Sorts the array into points order
 */
function printTable() {
    console.log("");
    console.log("Premier League predictor results after " + numGames + " games played.");
    console.log("");
    //Sorts all in the array by score
    all.sort(Comparator);

    //This prints the results in order
    for (i = 0; i < all.length; i++) {
        console.log(i + 1 + ". " + all[i].name + " with: " + all[i].score + " points");
    }
}

/**
 * Comparitor for sorting array by the score
 * @param {*} a First array
 * @param {*} b Second Array
 */
function Comparator(a, b) {
    if (a.score < b.score) return -1;
    if (a.score > b.score) return 1;
    return 0;
}
