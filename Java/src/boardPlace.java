@SuppressWarnings("rawtypes")
public class boardPlace implements Comparable {

	private int score;
	private String name;

	public boardPlace(int score, String name) {
		this.score = score;
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(Object comparePlace) {
		int compareScore = ((boardPlace) comparePlace).getScore();
		return this.score - compareScore;
	}

	public String toString() {
		return (name + " : " + score);
	}

}
