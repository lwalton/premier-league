import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;

public class main {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		ArrayList<boardPlace> theScores = new ArrayList<boardPlace>();

		// Premier League Table input
		// System.out.println("Table");
		String[] finishedLeague = new String[21];
		finishedLeague = importer.theImporter("2018test.txt");

		// System.out.println("Luke");
//		String[] Luke = new String[21];
//		Luke = importer.theImporter("Luke.txt");
//		boardPlace lukePlace = new boardPlace(compare.calc(Luke, finishedLeague), "Luke");
//		theScores.add(lukePlace);

		// System.out.println("Simon");
//		String[] Simon = new String[21];
//		Simon = importer.theImporter("Simon.txt");
//		boardPlace simonPlace = new boardPlace(compare.calc(Simon, finishedLeague), "Simon");
//		theScores.add(simonPlace);

		// System.out.println("Dad");
		String[] Graham = new String[21];
		Graham = importer.theImporter("Dad.txt");
		boardPlace graPlace = new boardPlace(compare.calc(Graham, finishedLeague), "Graham");
		theScores.add(graPlace);

		// System.out.println("Mum");
//		String[] Maria = new String[21];
//		Maria = importer.theImporter("Mum.txt");
//		boardPlace mumPlace = new boardPlace(compare.calc(Maria, finishedLeague), "Maria");
//		theScores.add(mumPlace);

		// System.out.println("George");
//		String[] George = new String[21];
//		George = importer.theImporter("George.txt");
//		boardPlace geoPlace = new boardPlace(compare.calc(George, finishedLeague), "George");
//		theScores.add(geoPlace);

		// System.out.println("Haulah");
//		 String[] Haulah = new String[21];
//		 Haulah = importer.theImporter("Haulah.txt");
//		 boardPlace haulahPlace = new boardPlace(compare.calc(Haulah,
//		 finishedLeague), "Haulah");
//		 theScores.add(haulahPlace);

		// System.out.println("Elena");
//		String[] Elena = new String[21];
//		Elena = importer.theImporter("Elena.txt");
//		boardPlace elPlace = new boardPlace(compare.calc(Elena, finishedLeague), "Elena");
//		theScores.add(elPlace);

		// System.out.println("Theo");
//		String[] Theo = new String[21];
//		Theo = importer.theImporter("Theo.txt");
//		boardPlace theoPlace = new boardPlace(compare.calc(Theo, finishedLeague), "Theo");
//		theScores.add(theoPlace);

		// System.out.println("Paps");
//		String[] PapaSi = new String[21];
//		PapaSi = importer.theImporter("PapaSi.txt");
//		boardPlace papsPlace = new boardPlace(compare.calc(PapaSi, finishedLeague), "Papa Si");
//		theScores.add(papsPlace);

		// System.out.println("Isaac");
//		String[] Isaac = new String[21];
//		Isaac = importer.theImporter("Isaac.txt");
//		boardPlace isaacPlace = new boardPlace(compare.calc(Isaac, finishedLeague), "Isaac");
//		theScores.add(isaacPlace);

		Collections.sort(theScores);

		System.out.println("The Premier League Predictor Table 2017/18:");
		System.out.println("(Lower Score is better)");
		for (int i = 1; i <= theScores.size(); i++) {
			System.out.println(i + ". " + theScores.get(i - 1).toString());
		}

		long end = System.currentTimeMillis();
		NumberFormat formatter = new DecimalFormat("#0.00000");
		System.out.println("Premier League execution time: " + formatter.format((end - start) / 1000d) + " seconds");

	}
}