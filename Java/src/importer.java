import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class importer {

	// This method imports the users table from txt and puts it into an array of
	// strings
	public static String[] theImporter(String fileName) {

		File theFile = new File(fileName);
		Scanner in = null;
		try {
			in = new Scanner(theFile);
		} catch (FileNotFoundException e) {
			System.out.println("Error - file not found");
		}

		String[] tempTable = new String[21];

		int x = 0;
		while (in.hasNextLine()) {
			x += 1;
			tempTable[x] = in.nextLine();
		}
		in.close();
		return tempTable;
	}

	// Not used...
	// public String[] guessedMaker(String fileName) {
	//
	// File theFile = new File(fileName);
	// Scanner in = null;
	// try {
	// in = new Scanner(theFile);
	// } catch (FileNotFoundException e) {
	// System.out.println("Error - file not found");
	// }
	//
	// String[] tempTable = new String[21];
	//
	// int x = 0;
	// while (in.hasNextLine()) {
	// x += 1;
	// tempTable[x] = in.nextLine();
	// // System.out.println(x + ". " + tempTable[x]);
	// }
	// in.close();
	// return tempTable;
	//
	// }
}
