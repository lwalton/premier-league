
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class MyDetails extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>

      <div class="container">
       <h4>Comparision table for [[person]]</h4> 
       <div class="insideContainer">
       <div class="tabletext float-left textright">
       <table id="persTable">
          <tbody>
          <template is="dom-repeat" items="[[personTable]]">
            <tr >
              <!-- <td>0</td> -->
              <td>[[item]]</td>    
            </tr>
           </template>
          </tbody>
        </table>
        </div>
        <div class="difftext">
          <table>
            <tbody>
            
              <template is="dom-repeat" items="[[difference]]">
                <tr>
                  <td>[[item]]</td>
                </tr>
              </template>
            </tbody>
        </table>
        
        </div>
       <div class=" tabletext float-left">
       <table id="actTable">
          <tbody>
          <template is="dom-repeat" items="[[actualTable]]">
            <tr >
              <!-- <td>0</td> -->
              <td>[[item]]</td>    
            </tr>
           </template>
          </tbody>
        </table>
        </div>
        </div>
      </div>
      <app-location route="{{route}}"></app-location>
      <iron-ajax auto
        id="ourAjax" 
        url="../data/prem.json"
        handle-as="json" 
        on-response="setOurJson">
      </iron-ajax>

      <iron-ajax auto
        url="https://www.footballwebpages.co.uk/league-table.json"
        params='{"comp":"1"}'
        handle-as="json"
        on-response="setTheirJson"
        debounce-duration="300">
      </iron-ajax>
    `;
  }

  static get properties() {
    return {
      route: {
        type: Object,
      },
      person: {
        type: String,
      },
      personTable: {
        type: Array,
        reflectToAttribute: true,
        notify: true,
      },
      actualTable: {
        type: Array,
        reflectToAttribute: true,
        notify: true,
      },
      difference: {
        type: Array,
        reflectToAttribute: true,
        notify: true,
      },

    }
  }

  connectedCallback() {
    super.connectedCallback();
    this.person = this.route.__queryParams.name;
  }

  setOurJson(event, res) {
    var arr1 = [];
    for (var i = 0; i < res.response.players.length; i++) {
      if (res.response.players[i].name === this.person) {
        arr1[0]="";
        for(var i0 = 1; i0 < 21; i0++){
           arr1[i0]  =  res.response.players[i].table[i0];
        }
        this.personTable = arr1;
        break;
      }
    }
    this.checkall();
  }

  setTheirJson(event, res) {
    var arr2 = [];
    arr2[0] = "";
    for (var i1 = 0; i1 < 20; i1++) {
      arr2[i1 + 1] = res.response.leagueTable.team[i1].name;
    }
    this.actualTable = arr2;

    var arr2 = [];
    arr2[0] = "";
    for (var i2 = 0; i2 < 20; i2++) {
      arr2[i2 + 1] = i2 + 1;
    }
    this.difference = arr2;

    this.checkall();
  }

  checkall() {
    if ((this.personTable != null) && (this.actualTable != null)) {
      this.comparer();
    }
  }


  comparer(){
    var perstable, acttable, pRows, aRows;

    perstable = this.shadowRoot.getElementById("persTable");
    acttable = this.shadowRoot.getElementById("actTable");

    pRows = perstable.rows;
    aRows = acttable.rows;

    for(var i3 = 1; i3<21 ;i3++){
      var pGuess, actualTeam;
      pGuess = this.personTable[i3];
      actualTeam = this.actualTable[i3];

      if(pGuess === actualTeam){
        
        pRows[i3].getElementsByTagName("TD")[0].style = "background-color: green;";
        // aRows[i3].getElementsByTagName("TD")[0].style = "background-color: green;";
        console.log("HELLO");
        //todo doesnt reload when different persons page is visited
      }
    }
  }
}

window.customElements.define('my-details', MyDetails);
