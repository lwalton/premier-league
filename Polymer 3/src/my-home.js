
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/iron-ajax/iron-ajax.js';
class HomeView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
      <div class="not-applied" id="errorBox">
      <h1 class="none" id="errorText">Something's gone wrong...</h1>
        <div class="container" id="theContainer">
        <h3>Premier League table</h3>
        <h5 id="num"> </h5>
        <h6>Lower is better</h6>
        
        <table class="striped"  id="myTable">
    
          <tbody>
            <template is="dom-repeat" items="[[all]]">
              <tr on-click="handleClick([[item.name]])" id="[[item.name]]">
            
                <td>0</td>
                <td>
                <a href="[[rootPath]]details?name=[[item.name]]">
                [[item.name]]
                </a>
                </td>
                <td>[[item.score]]</td>
              
              </tr>
            </template>
          </tbody>
        </table>
        </div>
        <button on-click="calculate" class="none">
  Calculate!
        </button>
        
        <button on-click="duplicates" class="none">
  Check for duplicates
        </button>
      </div>
      <iron-ajax auto 
        id="ourAjax" 
        url="../data/prem.json" 
        handle-as="json" 
        on-response="setOurJson">
      </iron-ajax>

      <iron-ajax auto
        url="https://www.footballwebpages.co.uk/league-table.json"
        params='{"comp":"1"}'
        handle-as="json"
        on-response="setTheirJson"
        on-error="error"
        debounce-duration="300">
      </iron-ajax>

    `;
  }
  static get properties() {
    return {

      numGames: {
        type: Number,
      },
      actual: {
        type: Object,
        value: {
        }
      },
      all: {
        reflectToAttribute: true,
        notify: true,
        type: Array,
      }
    };
  }

  constructor() {
    super();
  }

  setOurJson(event, res) {
    this.all = res.response.players;
    this.checkall();
  }

  error() {
    this.shadowRoot.getElementById("errorBox").classList.add("error-applied");
    this.shadowRoot.getElementById("theContainer").classList.add("hidden");
    this.shadowRoot.getElementById("errorText").classList.remove("none");
  }

  setTheirJson(event, res) {
    this.numGames = res.response.leagueTable.team[1].played;
    this.shadowRoot.getElementById("num").innerHTML = "Results after " + this.numGames + " gameweeks.";
    for (var i1 = 0; i1 < 20; i1++) {
      this.actual[i1 + 1] = res.response.leagueTable.team[i1].name;
    }
    this.checkall();
  }

  checkall() {
    if ((this.all != null) && (this.actual != null)) {
      this.calculate();
    }
  }

  duplicates() {
    for (var i2 = 0; i2 < this.all.length; i2++) {
      this.checkForDuplicates(this.all[i2].table);
    }
  }

  //Checks for duplicates in our tables.
  //Only needed when new tables are added
  checkForDuplicates(j) {
    var flag = false;
    var dupe;
    for (var i3 = 0; i3 < 19; i3++) {
      for (var i4 = i3 + 1; i4 < 20; i4++) {
        if (j[i3] === j[i4]) {
          dupe = "Duplicate found: " + j[i3] + " is the same as " + j[i4];
          console.log(dupe);
          flag = true;
          return;
        }
      }
    }
    if (flag) {
      alert(dupe);
    } else {
      alert("No duplicates found!");
    }
  }

  calculate() {
    //get scores
    for (var i5 = 0; i5 < this.all.length; i5++) {
      this.all[i5].score = this.comp(this.all[i5].table, this.actual);
    }

    //generate / print table
    this.printTable();
  }

  comp(guess, actual) {
    var length = 21;
    //COPIED FROM OLD CODE DIRECTLY
    var tempScore = 0;

    var errorBool = false;

    for (var i = 1; i <= 21 - 1; i++) {
      errorBool = false;
      for (var j = 1; j <= 21 - 1; j++) {
        if (actual[i] === guess[j]) {
          errorBool = true;
          if (i === j) {
          } else if (i > j) {
            var diff = i - j;
            tempScore += diff;
          } else if (j > i) {
            var diff = j - i;
            tempScore += diff;
          }
        }
      }
      if (errorBool === false) {
        // console.log("Team not found: " + actual[i].toString());
      }
    }
    return tempScore;
  }


  printTable() {
    //Sorts all in the array by score
    this.all.sort(this.Comparator);

    //This prints the results in order
    for (var i = 0; i < this.all.length; i++) {
      this.notifyPath("all." + i + ".score", this.all[i].score);
      this.notifyPath("all." + i + ".name", this.all[i].name);
    }


    var table, rows, switching, i, x, y, shouldSwitch;
    table = this.shadowRoot.getElementById("myTable");
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 1; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = rows[i].getElementsByTagName("TD")[2];
        y = rows[i + 1].getElementsByTagName("TD")[2];

        // Check if the two rows should switch place:
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
      }
    }

    for (var i = 0; i < table.rows.length; i++) {
      rows[i].getElementsByTagName("TD")[0].innerHTML = i + 1;
    }
  }

  Comparator(a, b) {
    if (a.score < b.score) return -1;
    if (a.score > b.score) return 1;
    return 0;
  }

  handleClick(name) {
    console.log(name);
  }
}

window.customElements.define('my-home', HomeView);