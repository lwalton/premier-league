import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import './my-icons.js';
import { Polymer } from '@polymer/polymer/polymer-legacy';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
        
      <style>
        :host {
          --app-primary-color: #7b1fa2 ;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }

        #back-button paper-icon-button{
         color:white;
       }
       #back-button:hover paper-icon-button{
         color:lightgrey;
       }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route
       route="{{route}}"
       pattern="[[rootPath]]:page" 
       data="{{routeData}}" 
       tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->          

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall" >
            <app-toolbar class="purple darken-2">
              <!-- <paper-icon-button id="back-button" icon="my-icons:arrow-back" drawer-toggle=""></paper-icon-button> -->
              <a href="[[rootPath]]" id="back-button">
              <paper-icon-button  icon="my-icons:arrow-back"></paper-icon-button>
              </a>
              <div main-title="">Premier League predictor</div>
            </app-toolbar>
          </app-header>


          <!-- <iron-pages selected="[[routeData.view]]" attr-for-selected="name" role="main">
          <my-home name="home" route="{{subroute}}"></my-home>
            <my-view2 name="view2" route="{{subroute}}"></my-view2>
            <my-view404 name="view404"></my-view404>
          </iron-pages> -->


          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <my-home name="home"></my-home>
            <my-details name="details"></my-details>
            <my-view404 name="view404"></my-view404>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
    // Show the corresponding page according to the route.
    //
    // If no page was found in the route data, page will be an empty string.
    // Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'home';
    } else if (['home', 'details'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    if(this.page == 'details'){
      this.shadowRoot.getElementById("back-button").style.visibility = "visible";
    } else {
      this.shadowRoot.getElementById("back-button").style.visibility = "hidden";
    }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'home':
        import('./my-home.js');
        break;
      case 'details':
        import('./my-details.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
  }
}

window.customElements.define('my-app', MyApp);
