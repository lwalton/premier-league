/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import '@polymer/polymer/polymer-element.js';

const $_documentContainer = document.createElement('template');
$_documentContainer.innerHTML = `<dom-module id="shared-styles">
  <template>
    <style>
      .card {
        margin: 24px;
        padding: 16px;
        color: #757575;
        border-radius: 5px;
        background-color: #fff;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
      }

      .circle {
        display: inline-block;
        width: 64px;
        height: 64px;
        text-align: center;
        color: #555;
        border-radius: 50%;
        background: #ddd;
        font-size: 30px;
        line-height: 64px;
      }

      h1 {
        margin: 16px 0;
        color: #212121;
        font-size: 22px;
      }

      .none{
        display: none;
      }
      
      tr{
        cursor:pointer;
      }

      tr:hover{
        background-color:#e0e0e0 !important ;
      }

      table{
        max-width: 800px !important;
      }

      #contentContainer{
        background-color: #7b1fa2; 
      }

      .hidden{
        opacity:0.2;
      }

      .error-applied{
        background-color:red !important;
        height:100%;
        width:100%;
        pointer-events: none;
        z-index:10;
        padding-top:60px;
        margin:0;
        position:absolute;
        top:0;
        left:0;
      }

      #errorText{
        width:100%;
        text-align: center;
        margin:auto;
        line-height:100%;
      }

      .float-left{
        float:left;
      }

      .float-right{
        float:right;
      }

      .tabletext{
        max-width: 250px;
      }

      .textright td{
        text-align: right !important;
      }

      .difftext{
        width:50px;
        float:left;  
      }

      .difftext td{
        text-align: center
      }

      .container{
        text-align: center;
      }
      .insideContainer{
        display: inline-block;
      }
    </style>
  </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
